/*@author Yannick Kosmehl FI-A-01 **/
package programm;
import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;


public class TwistUI extends Twist {

	private JFrame frmTwist;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TwistUI window = new TwistUI();
					window.frmTwist.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the application.
	 */
	public TwistUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTwist = new JFrame();
		frmTwist.setResizable(false);
		frmTwist.setTitle("Twist");
		frmTwist.setBackground(SystemColor.inactiveCaption);
		frmTwist.setBounds(100, 100, 800, 500);
		frmTwist.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTwist.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 287, 709, 147);
		frmTwist.getContentPane().add(scrollPane);
		
		JTextPane textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setBackground(SystemColor.control);
		textPane.setEditable(false);
		textPane.setText("Dieses Programm dient zum twisten und enttwisten von g�ngigen deutschen W�rtern.\n\nBedienung:\n"
				+ "Mit dem Button \"Twist\" kann der eingegebene Text getwistet werden. \nMit \"Decrypt\" kann der eingegebene Text enttwistet werden.\n"
				+ "Mit \"Clear\" wird der aktuelle Text aus dem Textfeld gel�scht. \nMit \"Example\" wird der Beipieltext wieder aufgerufen.\n\n"
				+ "Hinweise zum Enttwisten:\nEs d�rfen keine Sonderzeichen am Anfang des Orginalwortes stehen (z.B: �Paragraf, \"Anf�hrungszeichen\", (Klammern)).\n"
				+ "Es darf nur maximal 1 Sonderzeichen direkt hinter dem Orginalwort stehen (z.B. Hallo!  -> ist ok;   Hallo?!?  -> geht nicht).\n"
				+ "Stehen Sonderzeichen mitten im Wort (z.B. Binde-Strich), muss ein Leerzeichen gesetzt werden (-> Binde- Strich).\n"
				+ "\n\nBekannte Probleme: \n"
				+ "Eigennamen, Fremdw�rter und Numerische Zahlen werden beim enttwisten nicht in die richtige Reihenfolge gebracht, \n"
				+ "sondern unver�ndert ausgegeben. \n"
				+ "Es gibt W�rter mit Permutationen (mehrere W�rter, die sich aus denselben Buchstaben zusammensetzen lassen). In diesem Programm werden diese Permutationen "
				+ "alle unter dem selben Key in einer Liste in der Map gespeichert. Mein Programm ist nicht in der Lage aus de Kontext zu erkennen, welches der passenden W�rter"
				+ "vor dem Twisten das Originalwort war. Es entscheidet sich immer f�r die erste �bereinstimmung beim durchgehen der Liste. So auch im Beispieltext:\n"
				+ "\"anderen\" wird nach dem twisten und enttwisten zu \"anreden\", \"handelte\" wird zu \"haltende\"\n"
				+ "-->Der Anfangs-, Endbuchstabe und die Buchstaben dazwischen sind identisch, allerdings ist das Wort trotzdem ein anderes.");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(44, 91, 487, 173);
		frmTwist.getContentPane().add(scrollPane_1);
		JTextPane textArea = new JTextPane();
		scrollPane_1.setViewportView(textArea);
		String example = "Beispiel Text: "
				+ "Der griechische Begriff Kryptographie bedeutet Geheimschrift. Es handelt sich also um eine Wissenschaft, "
				+ "bei der es haupts�chlich um die Verschl�sselung von Texten und anderen Dateien geht. Dadurch soll der Zugriff "
				+ "f�r Unbefugte verhindert werden. Bereits im dritten Jahrtausend vor Christus wurden nachweislich die ersten Texte verschl�sselt. "
				+ "Sowohl im r�mischen Reich als auch im Mittelalter wurden wichtige Nachrichten oder Befehle mittels Kryptographie verschl�sselt. "
				+ "Julius Caesar nutzte f�r seine Nachrichten eine sehr simple Form der Kryptographie. Jeder Buchstabe wurde um den Buchstaben drei Stellen weiter "
				+ "im Alphabet ersetzt. Statt einem A wurde also ein D geschrieben. Der Empf�nger musste wissen, dass es sich um eine Verschiebung "
				+ "um drei Buchstaben handelte und konnte somit aus dem D wieder ein A machen. ";
		textArea.setText(example);
	
		JLabel Performance = new JLabel("Performance:");
		Performance.setBounds(417, 72, 333, 13);
		frmTwist.getContentPane().add(Performance);
		
		JButton btnNewButton = new JButton("Twist");
		btnNewButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				long start = System.currentTimeMillis();
				textArea.setText(Twist.twistText(textArea.getText()).toString());
				long ende = System.currentTimeMillis();
				long zeit = ende - start;
				Performance.setText("Performance: "+zeit+"ms");
			}
		});
		btnNewButton.setBounds(570, 91, 181, 33);
		frmTwist.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Decrypt");
		try {
			Twist.initDictionary();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				try {
					long start = System.currentTimeMillis();
					textArea.setText(Twist.solveTwistedText(textArea.getText()).toString());
					long ende = System.currentTimeMillis();
					long zeit = ende - start;
					Performance.setText("Performance: "+zeit+"ms");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(570, 161, 181, 33);
		frmTwist.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Clear");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				textArea.setText(null);
			}
		});
		btnNewButton_2.setBounds(570, 231, 86, 33);
		frmTwist.getContentPane().add(btnNewButton_2);
		
		JLabel lblNewLabel = new JLabel("Twist or decrypt your text");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(44, 31, 250, 33);
		frmTwist.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton_3 = new JButton("Example ");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				textArea.setText(example);
			}
		});
		btnNewButton_3.setBounds(666, 231, 85, 33);
		frmTwist.getContentPane().add(btnNewButton_3);
	}
}
