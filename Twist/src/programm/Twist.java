/*@author Yannick Kosmehl FI-A-01 **/

package programm;
import java.util.Scanner;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Twist {
/**Ich habe den Tip zum Fisher-Yates-Shuffle zu sp�t gelesen und bin von selbst 
   nicht auf den Algorithmus gekommen, deshalb habe ich mir eine L�sung mit ArrayList f�r die twist Funktion �berlegt*/
	
public static final String FILENAME = "C:\\Users\\Master\\git\\ls01_fi-x-01_yannick_kosmehl_rep\\Twist\\data\\wordlist.txt";
public static final Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>(100);

	public static void main(String[]args) throws IOException {		
		
		initDictionary();
		
		//zum Testen in der Konsole, ich empfehle jedoch eine Benutzung des UI
		System.out.println("Dieses Programm kann Ihre Nachrichten Twisten oder Enttwisten.\n"
				+ "Ich empfehle eine Benutzung �ber das Userinterface (TwistUI).\n");
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben Sie hier Ihre Nachricht ein:");
		String input = sc.nextLine();
		if(input.length()>3) {
				System.out.println("M�chten Sie ihre Nachricht Twisten, dann geben Sie 0 ein oder Enttwisten, dann geben Sie 1 ein:");
				int mode = sc.nextInt();
				if(mode==1) {
					System.out.print("Enttwistete Nachricht: \n"+solveTwistedText(input));
				}else if (mode==0) {
					System.out.print("Getwistete Nachricht: \n"+twistText(input));
				}else {
					System.out.println("Ungueltige Eingabe!>>> Twist (0) und Enttwisten (1)");
					main(args);
				}	
				System.out.println("\n\nWeitere Nachricht? Ja > (0) Nein > (1):");
				int x = sc.nextInt();
				if(x==0) {
					main(args);
				}else if(x==1){
					System.out.println("Auf Wiedersehen!");
				}else {
					System.out.println("Ungueltige Eingabe...");
				}
		}else {
			System.out.println("\nBitte geben Sie mindestens ein Wort mit 3 Buchstaben ein, da sonst kein twisten m�glich ist!");
			main(args);
		}
	}
	/**HINWEIS zur Laufzeit, Wahl der Veigleichsmethode:
	 * Um ein Wort wieder zu enttwisten k�nnte man nat�rlich einfach auf die bereits implementierten Methoden zur�ckgreifen und das Wort
	 * nochmals Twisten und dann mit der Wortliste abgleichen. Irgendwann wird durch das Vertauschen der Buchstaben zuf�llig das Originalwort 
	 * wieder erzeugt und man bekommt beim Abgleich mit der Wortliste ein Match. Das ist zwar simpel, f�hrt allerdings zu einer hohen und sehr 
	 * unkonstanten Laufzeit. Deshalb erstelle ich nochmal extra eine Map in der die W�rter in vielen verschiedenen Listen abgelegt sind. In einer
	 * Lite befinden sich jeweils die W�rter, die sich aus den exakt selben Buchstaben zusammensetzen, wodurch sich oft nur wenige oder sogar nur 
	 * ein einzelnes Wort in einer Liste befinden. Der Key mit denen die Listen aufgerufen werden, ergibt sich aus den aufsteigend sortierten Buchstaben
	 * der W�rter, die die jeweilige Liste beinhaltet. Das hei�t wenn ein Wort enttwistet werden soll, muss nur noch ein key nach demselben Prinzip erstellt
	 * werden und der Abgleich ist auf die jeweilige Liste mit diesem Key und damit nur einer handvoll m�glicher W�rter oder im optimalfall sogar nur einem
	 * einzigen passenden Wort eingegrenzt. Daraus resultiert eine wesentlich geringere Laufzeit und es bietet die spannende M�glichkeit das Programm daingehend 
	 * zu erweitern, dass beim Enttwisten nicht nur das erste �bereinstimmende Wort ausgegeben wird, sondern auch alle anderen �bereinstimmungen.
	 * So lassen sich z.B. aus den Buchstaben "adeennr" die W�rter "anreden" und "anderen" bilden. Da beide auch die selben Buchstaben am Anfang und Ende haben,
	 * kann man kontektlos nicht mehr sagen, welches vor dem Twist das Originalwort war. 
	 */
	
	/**@function W�rter werden mit UTF8-Encoding aus der W�rterliste gelesen und und in einer Liste gespeichert, dabei stehen alle W�rter die sich aus 
	 * deselben Buchstaben zusammensetzen in derselben Liste. Aus den Buchstaben der W�rter in einer Liste wird durch Sortierung in aufsteigeder Reihenfolge
	 * ein eindeutiger key generiert unter dem de jeweiligen Listen dann in einer Map gespeichert sind. Somit kann �ber den Key direkt ein gesuchtes Wort bzw. 
	 * die Liste an W�rtern mit desleben Buchstaben gefunden werden.
	 * 
	 * @throws IOException, wenn die wordlist.txt datei nicht gefunden wurde
	 */
	public static void initDictionary() throws IOException{
		 FileReader fr = null;
			try {
				fr = new FileReader(FILENAME);
			} catch (FileNotFoundException e) {
				System.out.println("Dictionary file nicht gefunden. Bitte den Pfad der wordlist.txt Datei �berpr�fen und ggf. den Pfad �ndern.");
			} 
			Scanner sc = new Scanner(fr);
			File fileDir = new File(FILENAME);

	        try {
				BufferedReader in = new BufferedReader(
				   new InputStreamReader(
				              new FileInputStream(fileDir), "UTF8")); //Wichtig: UTF8-Encoding damit W�rter mit Umlauten wie �,�, etc auch erkannt werden
				 String s;
		          while ((s = in.readLine()) != null) {  // solange Woerter vorhanden (in Datei immer 1 Wort pro Zeile), naechstes Wort einlesen
		        	  String sorted = sort(s);	         // Buchstaben in Wort sortieren -> Key erstellen
		              ArrayList<String> words = map.get(sorted);
		              if (words == null) {
		                  words = new ArrayList<String>();
		                  words.add(s);
		                  map.put(sorted, words);
		              } else {
		                  words.add(s);
		              } 
		          }
		          in.close();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	        sc.close();
	}
	/**@function Dient dazu einen Text in einzelne Woerter aufzuteilen (Trennung erfolgt durch Leerzeichen).
	 * Die einzelnen Woerter werden dann an @twistWord �bergeben und getwistet. Die Methode f�gt die getwisteten
	 * Woerter wieder zu einem Text zusammen und liefert diesen als Rueckgabewert.
	 * @param input
	 * @return getwisteten Text als StringBuilder Objekt */
	public static String twistText(String text) {
		String[] words = text.split(" ");		//Text in einzelne Woerter aufteilen und in einem Array Speichern
		StringBuilder sb = new StringBuilder (text.length());
		for(int i=0; i<words.length; i++) {		
			sb.append(twistWord(words[i])+" "); //alle Woerter des Arrays twisten und anschlie�end wieder zu einem Text zusammenf�gen
		}
		return sb.toString(); //StringBuilder Objekt in String umwandeln -> rueckgabe
	}
	/**@function dient zum twisten eines Wortes (erster und letzter Buchstabe bleiben gleich, 
	 * dazwischen wird nach dem Zufallsprinzip vertauscht.
	 * @param word das getwistet werden soll als String
	 * @return getwistetes Wort als String
	 */
	public static String twistWord(String word) {
		int length = word.length();
		StringBuilder sb = new StringBuilder (length);
		boolean equal = true;
		if(length>3) { // hat das Wort mehr als 3 zeichen? -> twistbar
			if(Character.isLetter(word.charAt(word.length()-1))) {  //endet das Wort auf einen Buchstaben? -> kein Satzzeichen vorhanden
				while(equal) {                                      //->solange bis twist erfolgreich war
					sb.delete(0,length);                            //StringBuilder leeren -> wichtig bei vorherigen Fehlversuchen
					sb.append(word.charAt(0));                      //erster Buchstabe bleibt gleich
					sb.append(twist(word.substring(1,length-1)));   //twisten der Buchstaben zwischen dem ersten und letzten Buchstaben
					sb.append(word.charAt(word.length()-1));        //zweiter Buchstabe bleibt gleich
						if(!(sb.toString().equals(word))) {         //testen ob Wort sich nach twist wirklich vom Originalwort unterscheidet 
					    //(durch Zufallsprinzip kann es sein, dass man "Pech" hat und die Buchstaben zufaellig genau wie im Originalwort angeordnet sind)
						equal = false;// wenn identisch-> neuer Versuch; nicht identisch -> twist erfolgreich -> Abbruch der Whileschleife
						}
				}
			}else if(length==4&&!(Character.isLetter(word.charAt(word.length()-1)))) { //Sonderfall 3 buchstaben + satzzeichen 
				sb.append(word); //-> direkt wieder ausgeben weil mit 3 Buchstaben nicht twistbar
			}else {              //mehr als 4 Zeichen mit Satzzeichen
				while(equal) {  
				//Prinzip wie oben, aber Satzzeichen beruecksichtigt
			    sb.delete(0,length);
				sb.append(word.charAt(0));
				sb.append(twist(word.substring(1,length-2)));			
				sb.append(word.charAt(word.length()-2));
				sb.append(word.charAt(word.length()-1));
					if(!(sb.toString().equals(word))) {
					equal = false;	
					}
				}
			}
		}else {
			sb.append(word);
		}
		return sb.toString();
	}
	
	/**@function vertauscht die Buchstaben des �bergebenen Strings nach dem Zufallsprinzip
	 * @param sub die zu vertauschenden Buchstaben als String
	 * @return die �bergebenen Buchstaben in vertauschter Reihenfolge als String
	 */
	public static String twist(String sub) {
		int random;
		StringBuilder sb = new StringBuilder (sub.length());
		ArrayList<String> list = new ArrayList<String>();
		
		for(int i =0;i<sub.length();i++) { //Buchstaben in Liste einfuegen
			list.add(i,String.valueOf(sub.charAt(i)));
		}
		
		for(int i = 0; i < sub.length(); i++) { //solange noch Buchstaben vorhanden
			random = (int) (Math.random()*list.size());
			sb.append(list.get(random));		//zufaelligen Buchstaben aus der Liste w�hlen und an neuen String anfuegen (durch StringBuilder Objekt)
			list.remove(random);                //Buchstaben aus Liste loechen, um Dopplung zu vermeiden
			}
		return sb.toString();
	}
	
	/**@function Dient zum enttwisten von Woertern 
	 * @param word zu enttwistendes Wort
	 * @return gibt gesuchtes Wort als String zurueck
	 */
	public static String solveTwistedWord(String word) {
	   
          int length = word.length();
  		  StringBuilder sb = new StringBuilder (length);
  		  String key;
  		  //key f�r gesuchtes Wort erzeugen
  		  if(Character.isLetter(word.charAt(length-1))) {  //pruefen ob Wort mit Buchstaben endet oder ob dahinter ein Satzzeichen steht
  			 key = sort(word); 
  		  }else {
  			 key=sort(word.substring(0,length-1));
  		  }
  		  ArrayList<String> matchingWords = map.get(key); //Liste mit passenden Woertern erzeugen (alle Elemente mit dem selben Key wie das zu enttwistende Wort!)
  		  
  		  if(matchingWords==null) {  //Falls keine Elemente f�r diesen Key vorhanden -> direkt Originalwort wieder ausgeben
  			sb.append(word);
  			return sb.toString();
  		  }
  		  
		  boolean missmatch = true;
  		  String temp;
  		  int size= matchingWords.size();
  		  
  		  //Abgleich mit den Elementen von matchingWords
          if(length>3&&Character.isLetter(word.charAt(length-1))) { //Wenn Wort auf Buchstaben endet direkt mit Dictionary abgleichen
        		  for(int i = 0; i<size;i++) {
        			  temp =matchingWords.get(i); //Alle Elemente der Liste f�r diesen Key durchgehen
        			  if(Character.toUpperCase(temp.charAt(0))==Character.toUpperCase(word.charAt(0))&&Character.toUpperCase(temp.charAt(temp.length()-1))==Character.toUpperCase(word.charAt(length-1)))  {
        				//Erster und letzter Buchstabe gleich? -> gesuchtes  Wort befunden ->Abgleich beenden
        				  sb.append(word.charAt(0));
        				  sb.append(temp.substring(1));
        				  missmatch = false;
        				  break;
        			  }
        		  }
          }else if(length>3&&!(Character.isLetter(word.charAt(length-1)))){ //Wenn kein Buchstabe am Ende (Satzzeichen) -> letztes Zeichen merken und f�r Abgleich mit Dictionary "abschneiden"
        	  for(int i = 0; i<size;i++) {
    			  temp =matchingWords.get(i);
    			  if(Character.toUpperCase(temp.charAt(0))==Character.toUpperCase(word.charAt(0))&&Character.toUpperCase(temp.charAt(temp.length()-1))==Character.toUpperCase(word.charAt(length-2))) {
    				  sb.append(word.charAt(0));
    				  sb.append(temp.substring(1));
    				  sb.append(word.charAt(length-1));
    				  missmatch = false;
    				  break;
    			  }
        	  }
        	  missmatch = false;
          }else {		//Falls Wort max 3 Buchstaben hat ist es nicht twistbar und kann direkt wieder ausgegeben werden
        	  sb.delete(0, length);
        	  sb.append(word);
        	  missmatch = false;
          }
          
          if(missmatch) { //Falls Abgleich mit Dictionary erfolglos, Originalwort wieder ausgeben, um Kontext nicht zu veraendern
        	  sb.append(word);
          }
		return sb.toString();
	}
	
	/**@function Dient dazu einen Text in einzelne Woerter aufzuteilen (Trennung erfolgt durch Leerzeichen).
	 * Die einzelnen Woerter werden dann an @solveTwistedWord �bergeben und enttwistet. Die Methode f�gt die enttwisteten
	 * Woerter wieder zu einem Text zusammen und liefert diesen als Rueckgabewert.
	 * @param input
	 * @return gel�sten Text als String
	 * @throws IOException 
	 */
	public static String solveTwistedText(String input) throws IOException {
		String[] words = input.split(" ");                    //Text in Woerter aufteilen
		StringBuilder sb = new StringBuilder(input.length());
		for(int i=0; i<words.length; i++) {
			sb.append(solveTwistedWord(words[i])+" ");        //Woerter enttwisten und wieder zu Text zusammenfuegen 
		}
		return sb.toString();
	}
	
	/**@function Wandelt einen String in Go�buchstaben und sortiert die Buchstaben
	 * in aufsteigender Reihenfolge. Dient dazu einen Key f�r das Wort nach dem 
	 * oben genannten Schema zu erzeugen.
	 * @param input optimalerweise ein Wort (nur Buchstaben)
	 * @return den key als String 
	 */
    public static String sort(String input) {
    	String word = input.toUpperCase();  //Gro�buchstaben um Inkonsistenz bzgl. Gro� und Kleinschreibung zu vermeiden (Gro�schreibung durch Satzanfang)
    	int length = word.length();
    	char[] letters = new char[length];  //Array erzeugen und Buchstaben einlesen
    	for(int i =0; i <length;i++) {
    	letters[i] = word.charAt(i);
    	}
        Arrays.sort(letters);				//Buchstaben im Array aufsteigend sortieren
        String key = new String (letters);
        return key;
    }
}