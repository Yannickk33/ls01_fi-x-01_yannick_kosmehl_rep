package uebungsaufgaben;

public class Aufgabe1 {
	
	public static void main (String []args) {
//		String datum = "9.11.";
//		System.out.println("heute ist der ");
		String s= "Java Programm";
		System.out.printf("\n|%s|\n", s);
		System.out.printf("|%20s|\n", s);    //rechtb�ndig mit 20 Stellen
		System.out.printf("|%-20s|\n", s);   //linkdsb�ndig mit 20 Stellen
		System.out.printf("|%5s|\n", s);	 //minimal 5 Stellen
		System.out.printf("|%.4s|\n", s);	 //maximal 4 Stellen
		System.out.printf("|%20.4s|\n\n", s);	 //20 Positionen, rechtsb�ndig, h�chstens 4 Stellen von String
		
		System.out.println("Aufgabe 1:");
		
		String a = "*";
		System.out.printf("\n%4s%s\n", a, a);
		System.out.printf("%s%7s\n", a, a);
		System.out.printf("%s%7s\n", a, a);
		System.out.printf("%4s%s\n", a, a);
		//                   Zeilenvorschub   %zeigt neue Formatierungsregel an   linksb�ndig mit 10 Stellen  maximal 3 Stellen sollen angezeigt werden   Datentyp s        Wert
		//System.out.printf("\n               %                                   -10                         .3                                             s",               x)
		
	}
}
