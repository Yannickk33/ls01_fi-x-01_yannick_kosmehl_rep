package uebungsaufgaben;

public class Aufgabe3Temperaturtabelle {
	
	public static void main (String []args) {
		
		final String x = "|";
		final String f = "Fahrenheit";
		final String c = "Celsius";
		final int f0 = 0;
		final int f1 = 10;
		final int f2 = 20;
		final int f3 = 30;
		final double c0 = 28.89;
		final double c1 = 23.33;
		final double c2 = 17.78;
		final double c3 = 6.67;
		final double c4 = 1.11;
		
		System.out.printf("%-12s%s%10s\n",f,x,c );
		System.out.println("-----------------------");
		System.out.printf("%-12d%s%10.2f\n",-f2,x,-c0 );
		System.out.printf("%-12d%s%10.2f\n",-f1,x,-c1 );
		System.out.printf("+"+"%-11d%s%10.2f\n",f0,x,-c2 );
		System.out.printf("+"+"%-11d%s%10.2f\n",f2,x,-c3 );
		System.out.printf("+"+"%-11d%s%10.2f\n",f3,x,-c4 );
	}
}
