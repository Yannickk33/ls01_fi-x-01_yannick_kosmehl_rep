/*
 * @author Yannick Kosmehl, s887234
 */
package programmierpflicht;

public class HashtabelleTest {

		public static void main(String[]args) {
			
			Hashtabelle schauspieler = new Hashtabelle();	//Hashtabelle erstellen und mit Wertepaaren f�llen
			schauspieler.put(0, "jhonny depp");
			schauspieler.put(1, "edward norton");
			schauspieler.put(2147483647, "alan rickman");
			schauspieler.put(2, "kevin spacey");
			schauspieler.put(3, "christian bale");
			schauspieler.put(4, "christoph walz");
			schauspieler.put(5, "harrison ford");
			
			System.out.println("Hashtabelle schauspieler:");		//pr�fe ob Wertepaare richtig eingef�gt wurden mit "get-Methode"	
			System.out.println();
			System.out.println(schauspieler.get(0));
			System.out.println(schauspieler.get(1));
			System.out.println(schauspieler.get(2147483647));
			System.out.println(schauspieler.get(2));
			System.out.println(schauspieler.get(3));
			System.out.println(schauspieler.get(4));
			System.out.println(schauspieler.get(5));
			System.out.println("pr�fe returnwert wenn key nicht vorhanden: "+schauspieler.get(6));
			System.out.println();
			
			System.out.println("Aktualisieren eines Wertepares mit \"put\":");		//pr�fe richtiges Aktualisieren von Wertepaaren mit "put-Methode"	
			System.out.println();
			System.out.print("vorher: ");
			System.out.println(schauspieler.get(1));
			System.out.println("test ob alter wert returnt wird: "+schauspieler.put(1, "Keanu Reeves"));
			System.out.print("nachher: ");
			System.out.println(schauspieler.get(1));
			System.out.println("test ob returnwert null bei neuem wertepaar: "+schauspieler.put(7, "Tom Hanks"));
			System.out.println();
			System.out.println();
			
			System.out.println("Testen der \"remove\" Funktion:");			//pr�fe remove-Mehtode
			System.out.println();
			System.out.println("pr�fe returnwert: "+schauspieler.remove(1));
			System.out.println("pr�fe ob returnwert null bei nicht vorhandenem key \"13\": "+schauspieler.remove(13));
			System.out.println("pr�fe ob wert entfernt ist: "+schauspieler.get(1));
		}
}
