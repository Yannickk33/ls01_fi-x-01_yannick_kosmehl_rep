/*
 * @author Yannick Kosmehl, s887234
 */
package programmierpflicht;

import java.util.LinkedList;

public class Hashtabelle implements IntStringMap{

    public static final int ArraySize = 128;
    private static LinkedList<KeyValuePair>[] hashtable;
 
    public class KeyValuePair {	//innere Klasse 
    	public String value;
        public Integer key;
        
        public KeyValuePair(Integer key, String value) {  //Konstruktor
        	this.key = key;
            this.value = value;
        }
    }
    public Hashtabelle() {		//initialisiere Array
    	hashtable = new LinkedList[ArraySize];
    }
    
    /**
     * @Funktion errechnet aus dem parameter key einen int wert index der im Array hashtabelle den index angibt
     * an dem sich die LinkedList befindet in die das Key Value Paar eingef�gt werden soll
     * @param key
     * @return index 
     */
    public int HashFunction(Integer key) {
    	int index = Math.abs(key.hashCode()%ArraySize);  
    	return index;
    }
    
    /**
     * @Funktion liefert das KeyValuePaar Objekt das dem �bergebenen Key entspricht
     * @param key
     * @return das KeyValuePaar als KeyVlauePair-Object oder null
     */
    public KeyValuePair getPair(Integer key) {		//get Funktion "aufgeteilt", damit man auch die M�glichkeit hat sich ein 
        if(key == null)								//keyvaluepaar zur�ckzugeben anstatt nur des String value teils
            return null;
 
        int index = HashFunction(key);
        LinkedList<KeyValuePair> linkedList = hashtable[index];  //kopie der LinkedList aus dem Array des entsprechenden index zu key
 
        if(linkedList == null)
            return null;
 
        for(KeyValuePair kvpair : linkedList) {			//for-each Iteration �ber alle kvpair's der linkedList
            if(kvpair.key.equals(key))
                return kvpair;
        }
        return null;
    }
 
    /**
	 * bestimmt den Wert zu einem Schluessel in der Map
	 * @param key der Schluessel
	 * @return den Wert zum Schluessel key, falls ein entsprechendes Schluessel-Wert-Paar vorhanden ist;
	 *   null sonst
	 */
    public String get(Integer key) {
    	KeyValuePair kvpair = getPair(key);
 
        if(kvpair == null) {
            return null;
        }else {
            return kvpair.value;
        }
    }
    
    /**
	 * fuegt ein Schluessel-Wert-Paar in die Map ein
	 * @param key der Schluessel
	 * @param value der Wert
	 * @return falls es schon einen Wert mit Schluessel key in der Map gab,
	 *   wird der alte Wert entfernt und zurueckgegeben; sonst wird null zurueckgegeben
	 */
    public String put(Integer key, String value) {
        int index = HashFunction(key);
        LinkedList<KeyValuePair> linkedList = hashtable[index];
 
        if(linkedList == null) {
        	linkedList = new LinkedList<KeyValuePair>();
 
            KeyValuePair kvpair = new KeyValuePair(key, value);
            linkedList.add(kvpair);				//einf�gen des neuen kvpair in die kopierte linked list
 
            hashtable[index] = linkedList;		//aktualisieren der linked list in der hashtabelle
        }
        else {
            for(KeyValuePair kvpair : linkedList) {
                if(kvpair.key.equals(key)) {
                	String valueAlt = kvpair.value;
                    kvpair.value = value;
                    return valueAlt;
                }
            }
            KeyValuePair kvpair = new KeyValuePair(key, value);
            linkedList.add(kvpair);
        }
        return null;
    }
    
    /**
	 * entfernt ein Schluessel-Wert-Paar aus der Map
	 * @param key der Schluessel
	 * @return der Wert zu dem Schluessel, falls ein entsprechendes Schluessel-Wert-Paar vorhanden ist;
	 *   null sonst
	 */
    public String remove(Integer key) {
    	
        int index = HashFunction(key);
        LinkedList<KeyValuePair> linkedList = hashtable[index];
        
        if(linkedList == null)
            return null;
        for(KeyValuePair kvpair : linkedList) {
            if (kvpair.key.equals(key)) {
            	linkedList.remove(kvpair);
                return kvpair.value;
            }
        }
		return null;       
    }   
}
