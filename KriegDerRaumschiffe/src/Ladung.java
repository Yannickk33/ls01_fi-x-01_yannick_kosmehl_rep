/**
 * Diese Klasse modelliert eine Ladung.
 * @author Yannick kosmehl
 *
 */
public class Ladung {

	//Attribute
	private String bezeichnung;
	private int menge;

	//Standardkonstruktor
	public Ladung () {

	}

	//Vollparametrisierter Konstruktor
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * Liefert die Bezeichnung der Ladung zur�ck.
	 * @return bezeichnung
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * Setzt den Wert des uebergebenen Arguments als Bezeichnung der Ladung.
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * Liefert die Menge der Ladung zur�ck.
	 * @return menge
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * Setzt den Wert des uebergebenen Arguments als Menge der Ladung.
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}

}