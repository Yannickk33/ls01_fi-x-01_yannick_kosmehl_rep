/**
 * Diese Klasse dient zum Testen der Klassen Raumschiff und Ladung.
 * @author Yannick Kosmehl
 *
 */
public class TestKlasse {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff(1, 100,100,100,100,2,"IKS Hegh'ta");
		Ladung schneckensaft = new Ladung ("Ferengi Schneckensaft", 200);
		Ladung schwert = new Ladung ("Bat'leth Klingonen Schwert", 200);
		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(schwert);
		
		Raumschiff romulaner = new Raumschiff(2, 100,100,100,100,2,"IRW Khazara");
		Ladung schrott = new Ladung ("Borg-Schrott", 5);
		Ladung plasmawaffe = new Ladung ("Plasma-Waffe", 50);
		Ladung rotematerie = new Ladung ("Rote Materie", 2);
		romulaner.addLadung(schrott);
		romulaner.addLadung(plasmawaffe);
		romulaner.addLadung(rotematerie);
		
		Raumschiff vulkanier = new Raumschiff(0, 80,80,50,100,5,"Ni'Var");
		Ladung torpedos = new Ladung ("Photonentorpedo", 3);
		Ladung sonden= new Ladung ("Forschungssonde", 35);
		vulkanier.addLadung(torpedos);
		vulkanier.addLadung(sonden);
		
		//Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.
		klingonen.photonentorpedoSchiessen(romulaner);
		//Die Romulaner schie�en mit der Phaserkanone zur�ck.
		romulaner.phaserkanoneSchiessen(klingonen);
		//Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		//Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("---------------------------------------------\n");
		//Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		//Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf.
		vulkanier.photonentorpedosLaden(3);
		vulkanier.landungsverzeichnisAufraeumen();
		System.out.println("---------------------------------------------\n");
		//Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		//Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("---------------------------------------------\n");
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("---------------------------------------------\n");
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println("---------------------------------------------\n");
		//Geben Sie den broadcastKommunikator aus.
		klingonen.eintraegeLogbuchKonsolenausgabe();
		romulaner.eintraegeLogbuchKonsolenausgabe();
		vulkanier.eintraegeLogbuchKonsolenausgabe();
		
	}

}
