import java.util.ArrayList;

/**
 * @author Yannick Kosmehl
 * @version 1.0
 */

/**
 * Diese Klasse modelliert ein Raumschiff.
 *
 */
public class Raumschiff {


	//Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> Ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Standartkonstruktor, parameterlos
	 */
	public Raumschiff() {
	}
	
	/**
	 * Konstruktor vollparametrisiert
	 * @param photonentorpedoAnzahl
	 * @param energieversorgungInProzent
	 * @param zustandSchildeInProzent
	 * @param zustandHuelleInProzent
	 * @param zustandLebenserhaltungssystemInProzent
	 * @param androidenAnzahl
	 * @param schiffsname
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent,int zustandSchildeInProzent,int zustandHuelleInProzent,
	int zustandLebenserhaltungssystemInProzent,int androidenAnzahl,String schiffsname) {
		
		this.photonentorpedoAnzahl=photonentorpedoAnzahl;
		this.energieversorgungInProzent=energieversorgungInProzent;
		this.schildeInProzent=zustandSchildeInProzent;
		this.huelleInProzent=zustandHuelleInProzent;
		this.lebenserhaltungssystemInProzent=zustandLebenserhaltungssystemInProzent;
		this.androidenAnzahl=androidenAnzahl;
		this.schiffsname=schiffsname;
		
	}
	
	/**
	 * Liefert die Anzahl der Photonentorpedos.
	 * @return photonentorpedoAnzahl
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	/**
	 * Setzt den Wert des uebergebenen Arguments als die Anzahl der Photonentorpedos.
	 * @param photonentorpedoAnzahlNeu
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	
	/**
	 * Liefert den Wert der Energieversorgung in Prozent.
	 * @return energieversorgungInProzent
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	
	/**
	 * Setzt den Wert des uebergebenen Arguments als Zustand der Energieversorgung in Prozent.
	 * @param energieversorgungInProzentNeu
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		if(energieversorgungInProzentNeu>=0)
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
		else
		this.energieversorgungInProzent = 0;	
	}
	
	/**
	 * Liefert den Zustand der Schilde in Prozent.
	 * @return schildeInProzent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	/**
	 * Setzt den Wert des uebergebenen Arguments als Zustand der Schilde in Prozent.
	 * @param zustandSchildeInProzentNeu
	 */
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	
	/**
	 * Liefert den Zustand der Huelle in Prozent.
	 * @return huelleInProzent
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	/**
	 * Setzt den Wert des uebergebenen Arguments als Zustand der Huelle in Prozent.
	 * @param zustandHuelleInProzentNeu
	 */
	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}

	/**
	 * Liefert den Zustand der Lebenserhaltungssysteme in Prozent.
	 * @return lebenserhaltungssystemInProzent
	 */
	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}
	
	/**
	 * Setzt den Wert des uebergebenen Arguments als Zustand der Lebenserhaltungssysteme in Prozent.
	 * @param zustandLebenserhaltungssystemInProzentNeu
	 */
	public void setLebenserhaltungssystemInProzent(int zustandLebenserhaltungssystemInProzentNeu) {
		this.lebenserhaltungssystemInProzent = zustandLebenserhaltungssystemInProzentNeu;
	}
	
	/**
	 * Liefert die Anzahl der Androiden.
	 * @return androidenAnzahl
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * Setzt den Wert des uebergebenen Arguments als Anzahl der Androiden.
	 * @param androidenAnzahlneu
	 */
	public void setAndroidenAnzahl(int androidenAnzahlneu) {
		this.androidenAnzahl = androidenAnzahlneu;
	}

	/**
	 * Liefert den Schiffsnamen
	 * @return schiffsname
	 */
	public String getSchiffsname() {
		return schiffsname;
	}
	
	/**
	 * Setzt das uebergebene Arguments als neuen Schiffsnamen.
	 * @param schiffsnameNeu
	 */
	public void setSchiffsname(String schiffsnameNeu) {
		this.schiffsname = schiffsnameNeu;
	}
	
	/**
	 * F�gt der ArrayList ladungsverzeichnis ein neues Objekt vom Typ Ladung hinzu.
	 * @param ladung
	 */
	public void addLadung(Ladung ladung) {
		Ladungsverzeichnis.add(ladung);
	}	

	/**
	 * Versucht einen Photonentorpedo auf das als Argument uebergebene Zielraumschiff zu schie�en. 
	 * Wenn keine Photonentorpedos vorhanden sind, wird die Nachricht "=*Click*=-" an alle gesendet.
	 * Ansonsten wird ein Photonentorpedo abgezogen, die Nachricht "Photonentorpedo abgeschossen" an alle gesendet und die Trefferfunktion aufgerufen.
	 * @param raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff raumschiff) {
		if(getPhotonentorpedoAnzahl()==0) {
			nachrichtAnAlle("=*Click*=-");
		}else {
		this.photonentorpedoAnzahl -=1;
		nachrichtAnAlle("Photonentorpedo abgeschossen");
		treffer(raumschiff);
		}
	}
	
	/**
	 * Versucht mit der Phaserkanone auf das als Argument uebergebene Zielraumschiff zu schie�en. 
	 * Wenn die Energieversogung unter 50% liegt, wird die Nachricht "=*Click*=-" an Alle gesendet.
	 * Ansonsten wird die Energiversorgung um 50 verringert, die Nachricht "Phaserkanone abgeschossen" an alle gesendet und die Trefferfunktion aufgerufen.
	 * @param raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff raumschiff) {
		if(this.energieversorgungInProzent<50) {
			nachrichtAnAlle("=*Click*=-");
		}else {
		this.energieversorgungInProzent -=50;
		nachrichtAnAlle("Phaserkanone abgeschossen");
		treffer(raumschiff);
		}
	}
	/**
	 * F�gt Schaden an den Schiffssystemen zu indem es deren Werte senkt. Wenn die Lebenserhaltungsysteme auf 0 sinken, wird eine entsprechende Nachricht dem Broadcastkommunikator hinzugef�gt.
	 * @param raumschiff
	 */
	private void treffer(Raumschiff raumschiff) {
		if(raumschiff.getSchildeInProzent()>50) {
			raumschiff.setSchildeInProzent(raumschiff.getSchildeInProzent()-50);
		}else {
			raumschiff.setSchildeInProzent(0);
			if(raumschiff.getHuelleInProzent()>50) {
			raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent()-50);
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent()-50);
			}else {
			raumschiff.setHuelleInProzent(0);
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent()-50);
			raumschiff.setLebenserhaltungssystemInProzent(0);
			nachrichtAnAlle("Lebenserhaltungssysteme wurden vernichtet.");
			
			}
		}
	}
	
	/**
	 * F�gt die als Argument uebergebene Nachricht der ArrayList broadcastKommunikator hinzu.
	 * @param nachricht
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
	}
	
	/**
	 * Liefert die ArrayList BroadcastKommunikator als R�ckgabewert.
	 * @return broadcastKommunikator
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;
	}
	
	/**
	 * Gibt die Elemente der ArrayList broadcastKomminukator als Logbucheintr�ge auf der Konsole aus.
	 */
	public void eintraegeLogbuchKonsolenausgabe(){
		System.out.println("\nLogbuch "+this.schiffsname+": ");
		 for(int i=0; i<broadcastKommunikator.size();i++) {
			 System.out.println("[Logbucheintrag "+i+"]: "+broadcastKommunikator.get(i));
			 }
		
	}
	/**
	 * Pr�ft ob Photonentorpedos entsprechend der gew�nschten Anzahl als Ladung vorhanden sind. 
	 * Falls ja werden sie als der Ladungsliste entfernt und in die Waffe des Schiffs eingesetzt.
	 * Falls nicht genug vorhanden sind, wird die maximale Menge eingesetzt. 
	 * Falls gar keine vorhanden sind, wird eine Nachricht ausgegeben.
	 * @param anzahlTorpedos
	 */

	public void photonentorpedosLaden(int anzahlTorpedos) {
		int menge = 0;
		for(int i=0; i<Ladungsverzeichnis.size();i++) {
			if(Ladungsverzeichnis.get(i).getBezeichnung().equals("Photonentorpedo")) {
				menge= Ladungsverzeichnis.get(i).getMenge();
				if(anzahlTorpedos>menge) {
					setPhotonentorpedoAnzahl(this.photonentorpedoAnzahl+menge);
					System.out.println(menge + "Photonentorpedo(s) eingesetzt");
					Ladungsverzeichnis.get(i).setMenge(0);
					break;
				}else {
					setPhotonentorpedoAnzahl(this.photonentorpedoAnzahl+anzahlTorpedos);
					System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
					Ladungsverzeichnis.get(i).setMenge(menge-anzahlTorpedos);
					break;
				}
			}else{
				System.out.println("Keine Photonentorpedos gefunden!");
				nachrichtAnAlle("=*Click*=-");
			}
		}
	
	} 
	
	/** 
	 * F�hrt Reparaturen der gew�nschten Schiffssysteme mit entsprechender �bergebener Anzahl an Adroiden aus.
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @param anzahlAndroiden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlAndroiden) {
		int zufallszahl=(int) ((Math.random()*(100)) + 1);
		int counter = 0;
		
		if(schutzschilde)
			counter +=1;
		if(energieversorgung)
			counter +=1;
		if(schiffshuelle)
			counter +=1;
		
		if(anzahlAndroiden>this.androidenAnzahl) {
			anzahlAndroiden=this.androidenAnzahl;
		}
		int berechnung = (zufallszahl*anzahlAndroiden)/counter;
		
		if(schutzschilde)
			setSchildeInProzent(this.schildeInProzent+berechnung);
		if(energieversorgung)
			setEnergieversorgungInProzent(this.energieversorgungInProzent+berechnung);
		if(schiffshuelle)
			setHuelleInProzent(this.huelleInProzent+berechnung);
	}
	
	/**
	 * Gibt die Attributwerte des Raumschiffs in der Konsole aus.
	 */
	public void zustandRaumschiff() {
		System.out.println("Schiffsname: "+getSchiffsname());
		System.out.println("AndroidenAnzahl: "+getAndroidenAnzahl());
		System.out.println("LebenserhaltungssystemInProzent: "+getLebenserhaltungssystemInProzent());
		System.out.println("HuelleInProzent: "+getHuelleInProzent());
		System.out.println("SchildeInProzent: "+getSchildeInProzent());
		System.out.println("EnergieversorgungInProzent: "+getEnergieversorgungInProzent());
		System.out.println("PhotonentorpedoAnzahl: "+getPhotonentorpedoAnzahl());
	} 

	/**
	 * Gibt die Ladungsliste des Raumschiffs auf der Konsole aus.
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungsverzeichnis: ");
		 for(int i=0; i<Ladungsverzeichnis.size();i++) {
			 System.out.print("Bezeichnung: "+Ladungsverzeichnis.get(i).getBezeichnung()+", ");
			 System.out.println("Menge: "+Ladungsverzeichnis.get(i).getMenge()); 
			 }
	 }
	
	/**
	 * Entfernt alle Ladungen mit der Menge 0 aus der Ladungsliste.
	 */
	public void landungsverzeichnisAufraeumen() {
		 for(int i=0; i<Ladungsverzeichnis.size();i++) {
			 if(Ladungsverzeichnis.get(i).getMenge()==0){
				 Ladungsverzeichnis.remove(i);
				 i--;
			 }
		 }
	 }
	}
	
	

