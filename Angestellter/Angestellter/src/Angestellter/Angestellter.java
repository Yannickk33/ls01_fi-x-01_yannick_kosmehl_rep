package Angestellter;

public class Angestellter {

	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 private String name;
	 private String vorname;
	 private double gehalt;
	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 
	 //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 public Angestellter(String name, String vorname, double gehalt) {
		 this.name=name;
		 this.vorname=vorname;
		 setGehalt(gehalt);
	 }
	 //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.
	 public Angestellter(String vorname, String name) {
		 setName(name);
		 setVorname(vorname);
	 }
	 
	 public void setName(String name) {
	    this.name = name;
	 }
	 
	 public String getName() {
	    return this.name;
	 }
	 
	 public void setVorname(String vorname) {
		    this.vorname = vorname;
		 }
		 
		 public String getVorname() {
		    return this.vorname;
		 }
	 

	 public void setGehalt(double gehalt) {
	   if(gehalt>0)
		   this.gehalt=gehalt;
	   else
		   this.gehalt=10;
		   System.out.println("Das Gehalt muss ein positiver Wert sein");
	 }
	 //TODO: 2. Implementieren Sie die entsprechende get-Methoden.
	 public double getGehalt() {
		 return this.gehalt;
	 }
	//TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
	 public String vollname() {
		 return this.vorname + " " + this.name;
	 }
}
