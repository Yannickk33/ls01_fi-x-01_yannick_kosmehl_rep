﻿//@author Yannick Kosmehl
import java.util.Scanner;
import java.util.InputMismatchException;

class Fahrkartenautomat{
	
  //Klassenvariablen
  public static double zuZahlenderBetrag; 
  public static double eingezahlterGesamtbetrag;
  public static double eingeworfeneMünze;
  public static double rückgabebetrag;
  public static short  anzahl;
  public static int FahrscheineMax =10;
  public static String[] fahrschein = new String[FahrscheineMax];
  public static double[] preis= new double[FahrscheineMax];
  
  
    public static void main(String[] args) {
    	while(true) {  
    	// nach abgeschlossendem Bestellvorgang steht der Automat direkt für die nächste Bestellung bereit
    	zuZahlenderBetrag = 0;
    	eingezahlterGesamtbetrag = 0;
    	eingeworfeneMünze = 0;
    	rückgabebetrag = 0;
    	anzahl = 0;
    	//Reset der Variablen
    	fahrkartenbestellungErfassen();
    	fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
    	//Methodenaufrufe für den Bestellvorgang
    	}
    }
    
    /**@function gibt das Rückgeld in Münzen aus, bis der rückgabebetrag gedeckt ist
     * @param eingezahlterGesamtbetrag, zuZahlenderBetrag
     */
    public static void rueckgeldAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        //Prüfung ob passend Bezahlt wurde
        {
     	   System.out.printf("%s%.2f%s","Der Rückgabebetrag in Höhe von " , rückgabebetrag , " EURO ");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   	//Ausgabe des Rückgelds in der geringsten möglichen Anzahl an Münzen
            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	 muenzeAusgeben(2, "EURO");
         	  rückgabebetrag= runden(rückgabebetrag -= 2.0);
         	  //Berechnung des Rückgabebetrags mit Rundung auf 2 Stellen nach dem Komma um Fehler bei der Rückgeldausgabe zu vermeiden
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  muenzeAusgeben(1,"EURO");
         	  rückgabebetrag= runden(rückgabebetrag -= 1.0);
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  muenzeAusgeben(50,"CENT");
         	  rückgabebetrag= runden(rückgabebetrag -= 0.5);
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20,"CENT");
         	  rückgabebetrag= runden(rückgabebetrag -= 0.2);
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10,"CENT");
         	  rückgabebetrag= runden(rückgabebetrag -= 0.1);
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5,"CENT");
         	  rückgabebetrag= runden(rückgabebetrag -= 0.05);
            }
        }
        
        //Ausgabe Abschlussnachricht
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    }
    
    /**@function nutzer wird aufgefordert Geldstücke von 2€ bis 5ct einzugeben, bis der zu bezahlende Betrag gedeckt ist
     * @param zuZahlen
     * @return eingezahlterGesamtbetrag
     */
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	eingezahlterGesamtbetrag = 0.0;
    	
    	//Ausgabe der Zahlungsaufforderung, bis der zu zahlende Betrag gedeckt ist
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: ", zuZahlen - eingezahlterGesamtbetrag, " EURO");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   //Eingabe welche Münze
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           //Berechnung
        }
		return eingezahlterGesamtbetrag;
    }
    public static void addFahrschein(int nr, String name, double pr) {
    	preis[nr]=pr;
    	fahrschein[nr]=name; 
    }
    /**@function nimmt Bestellungen von Fahrkarten und deren Anzahl entgegen bis der Nutzer Bezahlen (9) wählt
     */
    public static void fahrkartenbestellungErfassen() {
    	
    	//Fahrscheine hinzufügen
    	addFahrschein(0,"Einzelfahrschein Berlin AB",2.90);
    	addFahrschein(1,"Einzelfahrschein Berlin BC",3.30);
    	addFahrschein(2,"Einzelfahrschein Berlin ABC",3.60);
    	addFahrschein(3,"Kurzstrecke",1.90);
    	addFahrschein(4,"Tageskarte Berlin AB",8.60);
    	addFahrschein(5,"Tageskarte Berlin BC",9.00);
    	addFahrschein(6,"Tageskarte Berlin ABC",9.60);
    	addFahrschein(7,"Kleingruppen-Tageskarte Berlin AB",23.50);
    	addFahrschein(8,"Kleingruppen-Tageskarte Berlin BC",24.30);
    	addFahrschein(9,"Kleingruppen-Tageskarte Berlin ABC",24.90);
        
        //Ausgabe der Bestellübersicht
        System.out.println("Fahrkartenbestellvorgang:");
        System.out.println("=========================\n");
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        for(int i=0; i<fahrschein.length; i++) {
        System.out.printf("%-35s [%5.2f EUR] [%d]\n", fahrschein[i], preis[i], i);
        }
        System.out.printf("%-47s%s\n\n","Bestellung abschließen und bezahlen", "[111]");

    	//Fahrscheinbestellung
    	Scanner tastatur = new Scanner(System.in);
    	boolean ungueltig = true; 
    	int counter = 0;
        int eingabe = 0;
        int anzahl = 0;
        
        while(ungueltig) {						
       //Bestellungen werden solange angenommen bis bezahlen (111) gewählt wird
        	System.out.println("Ihre Wahl:");
        	eingabe = tastatur.nextInt();
        	if(eingabe<fahrschein.length && eingabe>=0 && !(eingabe==111)) {
        		System.out.println("Anzahl:");
        		anzahl = tastatur.nextInt();
        		
        		if(anzahl>0 && anzahl<=10) {
        			counter = counter + anzahl;
        			if(counter<=10) {
        				zuZahlenderBetrag = zuZahlenderBetrag + preis[eingabe]*anzahl;
        			}else {
        				System.out.println("Sie können maximal nur 10 Tickets pro Bestellung kaufen."
        									+ "Aktuell haben Sie breits "+counter+ "Tickets im Warenkorb.");
        			}
        		}else {
        			System.out.println(">>>> Flasche Eingabe <<<<");
        		}
        	}else if(eingabe == 111) {
        		//bezahlen
        		fahrkartenBezahlen(zuZahlenderBetrag);
        		ungueltig = false;
        	}else {
        		System.out.println(">>>> Falsche Eingabe <<<<");
        	}
        }	
    }
    
    /**@function Ausgeben der Fahrkarte mit Wartezeit
     */
    public static void fahrkartenAusgeben () {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    /**@function Ausgeben des Betrags der Münze mit Einheit für die Rückgeldausgabe
     */
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag+" "+ einheit);
    }
    
    /**@function Erzeugen einer Wartezeit für die Fahrscheinausgabe
     */
    public static void warte(int millisekunden) {
    	try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    /**@function Rundet eine Zahl auf zwei stellen nach dem Komma
     * @param zahl	
     * @return erg
     */
    public static double runden (double zahl) {
    	double temp = zahl*100;
    	long temp2 = Math.round(temp);
    	double erg = (double)temp2/100;
    	return erg;
    }
}