package zaehlschleifenAB1;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Twist {
/**Ich habe den Tip zum Fisher-Yates-Shuffle zu sp�t gelesen und bin von selbst 
   nicht auf den Algorithmus gekommen, deshalb die L�sung mit ArrayList*/
	
public static final String FILENAME = "C:\\Users\\Master\\git\\ls01_fi-x-01_yannick_kosmehl_rep\\Kontrollstrukturen\\data\\wordlist.txt";
public static final Map<String, Set<String>> dictionary =
new HashMap<String, Set<String>>(250000);

	public static void main(String[]args) {		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte Wort oder Satz eingeben:");
		String input= sc.nextLine();
		twistText(input);
		//solveTwistedText(input);
		sc.close();
	}
	public static void twistText(String sentence) {
		String[] words = sentence.split(" ");
		
		for(int i=0; i<words.length; i++) {
			System.out.print(twistWord(words[i]));
			System.out.print(" ");
		}
	}
	public static StringBuilder twistWord(String word) {
		int length = word.length();
		StringBuilder sb = new StringBuilder (length);
		boolean equal = true;
		if(length>3&&Character.isLetter(word.charAt(word.length()-1))) { //Sonderfall 3 buchstaben + satzzeichen 4 zeichen wird nicht ausgegeben
			if(Character.isLetter(word.charAt(word.length()-1))) {
				while(equal) {
					sb.delete(0,length);
					sb.append(word.charAt(0));
					sb.append(twist(word.substring(1,length-1)));
					sb.append(word.charAt(word.length()-1));
						if(!(sb.toString().equals(word))) {
						equal = false;
						}
				}
			}else {
				while(equal) {
			    sb.delete(0,length);
				sb.append(word.charAt(0));
				sb.append(twist(word.substring(1,length-2)));
				sb.append(word.charAt(word.length()-2));
				sb.append(word.charAt(word.length()-1));
					if(!(sb.toString().equals(word))) {
					equal = false;	
					}
				}
			}
		}else {
			sb.append(word);
		}
		return sb;
	}
	public static StringBuilder twist(String sub) {
		int random;
		StringBuilder sb = new StringBuilder (sub.length());
		ArrayList<String> list = new ArrayList<String>();
		
		for(int i =0;i<sub.length();i++) {
			list.add(i,String.valueOf(sub.charAt(i)));
		}
		
		for(int i = 0; i < sub.length(); i++) {
			random = (int) (Math.random()*list.size());
			sb.append(list.get(random));
			list.remove(random);
			}
		return sb;
	}
	public static String solveTwistedWord(String word) {

	      FileReader fr = null;
		try {
			fr = new FileReader(FILENAME);
		} catch (FileNotFoundException e) {
			System.out.println("file not found.");
		} 
		Scanner sc = new Scanner(fr);
		boolean missmatch = true;
		String solution = null;
		
		//Dictionary erstellen
		  String s;
          while ((s = sc.nextLine()) != null) {
              String sorted = sort(s);
              Set<String> words = dictionary.get(sorted);
              if (words == null) {
                  words = new TreeSet<String>();
                  words.add(s);
                  dictionary.put(sorted, words);
              } else {
                  words.add(s);
              } 
          }
        if(word.length()>3&&Character.isLetter(word.charAt(word.length()-1))) {
        	if(Character.isLetter(word.charAt(word.length()-1))) {
        		//if(sort(word) : dictionary.get(sort(word))) {
        	}else {
        		//word in substring ohne sonderzeichen enttwisten und wieder zusammenf�gen
        	}
        }else {
        	System.out.print(word+" ");
        }
     
		sc.close();
		return solution;
	}
	public static void solveTwistedText(String input) {
		String[] words = input.split(" ");
		
		for(int i=0; i<words.length; i++) {
			System.out.print(solveTwistedWord(words[i])+" ");
		}
	}
    public static String sort(String s) {
        byte[] letters = s.getBytes();
        Arrays.sort(letters);
        return new String(letters);
    }

}