package fallunterscheidungen;

import java.util.Hashtable;
import java.util.Scanner;

public class RoemischeZahlen{
 
  
    static Hashtable<Character, Integer> htRoemToDez = new Hashtable<>();
     
    public static void main(String[] args) {

        htRoemToDez.put('I', 1);
        htRoemToDez.put('V', 5);
        htRoemToDez.put('X', 10);
        htRoemToDez.put('L', 50);
        htRoemToDez.put('C', 100);
        htRoemToDez.put('D', 500);
        htRoemToDez.put('M', 1000);
         
        Scanner scanner = new Scanner(System.in);
        String input;
         
        do {
            System.out.println("Geben Sie eine Zahl in r�mischer schreibweise ein. \n"
            		+ "oder \"exit\" zum beenden des Programms:");
            input = scanner.nextLine().trim();
            
            if ("exit".equals(input)) {
                System.out.println("Programm wird beendet.......");
                System.exit(0);
            }
            else if (IsRoemisch(input)) {
                	System.out.println("In Dezimal: "+roemischZuDezimal(input));
            }else {
            	System.out.println("Die zahl war inkorrekt.");
            }
        }while(true);
    }
     
    public static Integer roemischZuDezimal(String roemisch) {
    	
        Integer erg = 0;
        int temp1 = 0;
        int temp2 = 1;
         
        for (int i = 0; i < roemisch.length(); i++) {
            temp1 = htRoemToDez.get(roemisch.charAt(i));
            if (i < roemisch.length() - 1) {
                temp2 = htRoemToDez.get(roemisch.charAt(i + 1));
            }
            else 
                temp2 = 0;
            
            if (temp1 < temp2) {
                erg += temp2 - temp1;
                i++;
            }
            else 
                erg += temp1;
        }
        return erg;
    }
     
    public static Boolean IsRoemisch(String value) {
       
        return true;
    }    
         
}