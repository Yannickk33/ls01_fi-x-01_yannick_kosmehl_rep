package fallunterscheidungen;
import java.util.Scanner;
public class OhmschesGesetz {

	public static void main(String[] args) {
		elektrischeGrundgrößenRechner();
	}
	public static void elektrischeGrundgrößenRechner() {
		
		System.out.println("Bitte geben geben Sie eine der der Größen I, U oder R ein, die Sie berechnen wollen:");
		Scanner sc = new Scanner(System.in);
		char größe = sc.next().charAt(0);
		
		double i, r, u;
		
		switch (größe) {
		case 'I':
			System.out.println("Spannung U in Volt:");
			u = sc.nextDouble();
			System.out.println("Widerstand R in Ohm:");
			r = sc.nextDouble();
			System.out.println("Die Stromstärke beträgt "+(u/r)+" Ampere.");
			break;
		case 'R':
			System.out.println("Spannung U in Volt:");
			u = sc.nextDouble();
			System.out.println("Stromstärke I in Ampere:");
			i = sc.nextDouble();
			System.out.println("Der Widerstand beträgt "+(u/i)+" Ohm.");
			break;
		case 'U':
			System.out.println("Widerstand R in Ohm:");
			r = sc.nextDouble();
			System.out.println("Stromstärke I in Ampere:");
			i = sc.nextDouble();
			System.out.println("Der Widerstand beträgt "+(r*i)+" Ohm.");
			break;
		default: 
			System.out.println("Sie haben keine valide Größe eingegeben.");
			break;
	}
		sc.close();

}
}
