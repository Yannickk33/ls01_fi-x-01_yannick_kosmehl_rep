package fallunterscheidungen;
import java.util.Scanner;
public class Schaltjahr {

	public static void main(String[] args) {
		vergleichen(eingabe());

	}
	public static int eingabe() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte Jahreszahl eingeben: ");
		int jahr = sc.nextInt();
		sc.close();
		return jahr;
	}
	
	public static void vergleichen(int jahr) {
		
		if(jahr<1582) {
			if(jahr%4==0)
				System.out.println("Das Jahr "+jahr+" ist ein Schaltjahr.");
			else
				System.out.println("Das Jahr "+jahr+" ist kein Schaltjahr.");
		}
		if(jahr>=1582) {
			if(jahr%4==0&&jahr%100!=0)
				System.out.println("Das Jahr "+jahr+" ist ein Schaltjahr.");
			if(jahr%100==0&&jahr%400!=0)
				System.out.println("Das Jahr "+jahr+" ist kein Schaltjahr.");
			if(jahr%400==0)
				System.out.println("Das Jahr "+jahr+" ist ein Schaltjahr.");
			if(jahr%4!=0)
				System.out.println("Das Jahr "+jahr+" ist kein Schaltjahr.");
		}
	}

}
