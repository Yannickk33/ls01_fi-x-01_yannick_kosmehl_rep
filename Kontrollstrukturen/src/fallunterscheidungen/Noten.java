package fallunterscheidungen;
import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		notenBezeichnung();
	}
		public static void notenBezeichnung() {
		int zahl;
		
		System.out.println("Geben Sie eine Note ein: ");
		Scanner tastatur = new Scanner(System.in);
		zahl = tastatur.nextInt();
		tastatur.close();
		
		if(0<zahl&&zahl<7){
			if(zahl==1) 
				System.out.println("Sehr gut.");
			if (zahl==2)
				System.out.println("Gut.");
			if(zahl==3) 
				System.out.println("Befriedigend.");
			if (zahl==4)
				System.out.println("Ausreichend.");
			if(zahl==5) 
				System.out.println("Mangelhaft.");
			if (zahl==6)
				System.out.println("Ungenügend.");
			
				
	}else 
		System.out.println("\"Error\": Bitte geben Sie eine Note zwischen 1 und 6 ein.");
	
	}
}
