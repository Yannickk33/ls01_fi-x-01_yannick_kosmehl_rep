package fallunterscheidungen;
import java.util.Scanner;

public class Monate {
	public static void main (String[]args) {
		monate();
	}
	public static void monate() {
		Scanner sc =new Scanner(System.in);
		System.out.println("Geben sie eine Zahl zwischen 1 und 12 ein, um zu sehen welchem Monat sie entspricht:");
		int monat=sc.nextInt();
		sc.close();
		
		switch(monat) {
		case 1 :
			System.out.println("Januar");
			break;
		case 2 :
			System.out.println("Februar");
			break;
		case 3 :
			System.out.println("Maerz");
			break;
		case 4 :
			System.out.println("April");
			break;
		case 5 :
			System.out.println("Mai");
			break;
		case 6 :
			System.out.println("Juni");
			break;
		case 7 :
			System.out.println("Juli");
			break;
		case 8 :
			System.out.println("August");
			break;
		case 9 :
			System.out.println("September");
			break;
		case 10 :
			System.out.println("Oktober");
			break;
		case 11:
			System.out.println("November");
			break;
		case 12:
			System.out.println("Dezember");
			break;
		default:
			System.out.println("Bitte nur zahlen von 1 bis 12.");
			break;
				
		}
	}
}
