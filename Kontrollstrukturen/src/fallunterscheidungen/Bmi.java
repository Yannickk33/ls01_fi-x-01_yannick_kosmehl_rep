package fallunterscheidungen;
import java.util.Scanner;
public class Bmi {

	public static void main(String[] args) {
		
		double gw, gr;
		String geschlecht;
		
		//Eingabe
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie ihre Gr��e in Metern an: ");
		gr = sc.nextDouble();
		System.out.println("Bitte geben Sie ihr Gewicht in Kilogramm an: ");
		gw = sc.nextDouble();
		System.out.println("Bitte geben Sie ihr Geschlecht an m f�r m�nnlich oder w f�r weiblich: ");
		geschlecht = sc.next();
		ausgabe(vergleichen(berechnung(gr, gw),geschlecht));
		sc.close();
	}
	
	
	public static double berechnung(double gr, double gw) {
		
		
		return gw/(gr*gr);
	}
	public static String vergleichen(double ergebnis, String geschlecht) {
		String klassifikation = null;
		String m= "m";
		String w="w";
		if(geschlecht.equals(m)) {
			if(ergebnis<19) 
				klassifikation = "Untergewicht";
			if(ergebnis<=24 && ergebnis>=19)
				klassifikation = "Normalgewicht";
			if(ergebnis>24)
				klassifikation = "�bergewicht";
		}else if(geschlecht.equals(w)) {
			if(ergebnis<20) 
				klassifikation = "Untergewicht";
			if(ergebnis<=25 && ergebnis>=20)
				klassifikation = "Normalgewicht";
			if(ergebnis>25)
				klassifikation = "�bergewicht";
		}
		return klassifikation;
	}
	public static void ausgabe (String klassifikation) {
		if(klassifikation=="Untergewicht") {
			System.out.println("Sie haben "+klassifikation);
		}else if(klassifikation=="Normalgewicht") {
			System.out.println("Sie haben "+klassifikation);
		}else if(klassifikation=="�bergewicht") {
			System.out.println("Sie haben "+klassifikation);
		}
	}

}
