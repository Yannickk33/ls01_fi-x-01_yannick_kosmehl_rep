package fallunterscheidungen;
import java.util.Scanner;
public class Zahlenvergleich {
	
	public static void main (String[]args) {
		vergleicheZweiInteger();
	}
	public static void vergleicheZweiInteger() {
		//Deklaration der Variablen
		int z1, z2;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die erste Zahl ein: ");
		
		//Initialisierung der Variablen
		z1 = sc.nextInt();
		System.out.println("Bitte geben Sie die zweite Zahl ein: ");
		z2 = sc.nextInt();
		sc.close();
		
		if(z1 < z2) 
			System.out.println("Die erste Zahl ist kleiner als die zweite Zahl.");
		else if(z1 > z2) 
			System.out.println("Die erste Zahl ist gr��er als die zweite Zahl.");
		else 
			System.out.println("Die Zahlen sind gleich gro�.");
	}
}
