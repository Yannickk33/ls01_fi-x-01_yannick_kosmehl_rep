//@author Yannick Kosmehl
package Uebungen;

public class Textverschlüsselung {
//gewählte Verschlüsselungsmethode ist der Caesar Algorithmus
	
	public static void main(String[] args) {
		  //kurze Testausgabe
		  String klartext = "Verschluesseln test";
		  System.out.println(encrypt(klartext));
		  System.out.println(decrypt(encrypt(klartext)));
	}
	    public static char[] encrypt(String klartext) {
	    /**sinnvolle Ergänzung wäre es zusätzlich den Schlüssel, also den Wert mit dem verschlüsselt werden soll (hier default 3)
	       ebenfalls als Parameter zu übergeben, aber die Aufgabenstellung verlangt ja nur den Klartext als parameter
	       z.B. so: encrypt(String klartext, int key)*/	
	    	char[] charArray = new char[klartext.length()];
	    	for(int i =0; i<klartext.length(); i++) {
	    		charArray [i]=klartext.charAt(i);
	    	}
	    	
	        char[] encryptArray = new char[charArray.length]; //leeres Array erstellen
	 
	        for (int i = 0; i < charArray.length; i++) {
	                    int verschiebung = (charArray[i] + 3)%128; // ursprüngliches Zeichen plus 3 modulo 128, damit keine zeichen über 128 kommen
	                    encryptArray[i] = (char) (verschiebung);		//cast nach char um fehler zu vermeiden
	        }
	        return encryptArray;
	    }
	 
	    public static char[] decrypt(char[] charArray) {
	 
	        char[] cryptArray = new char[charArray.length];
	        int verschiebung; 
	 
	        for (int i = 0; i < charArray.length; i++) {
	 
	                    if (charArray[i] - 3 < 0) {
	                    	verschiebung = charArray[i] - 3 + 128;		//wenn wert- verschiebung < 0, dann +128 rechnen
	                    }else {
	                    	verschiebung = (charArray[i] - 3)%128;      //wenn nein dan einfach nur modulo 128
	                    }
	                     cryptArray[i] = (char) (verschiebung);
	        }
	        return cryptArray;
	    }
}
