package Uebungen;
import java.util.Scanner;

public class Summe {
	
	public static void main(String[]args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein, bis zu welcher addiert werden soll:");
		int sumBis=0;
		int summe=0;
		int zaehler =0;
		sumBis = sc.nextInt();
		
		//Kopfgesteuerte Schleife
		
		while(zaehler <= sumBis) {
			summe += zaehler;
			zaehler++;
		}
		System.out.println(summe);
		
		summe = 0;
		zaehler = 0;
		
		do {
			summe += zaehler;
			zaehler++;
			
		}while(zaehler <=sumBis);
		System.out.println(summe);
		summe = 0;
		zaehler = 0;
	
	for(int i=0; i <=sumBis;i++) {
		summe += i;
	}
	System.out.println(summe);
	}
}
