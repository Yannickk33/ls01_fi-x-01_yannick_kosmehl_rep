package Uebungen;

public class ArraysUB4Lotto {
	
	public static void main (String []args) {
		
		final int[] lottozahlen ={3,7,12,18,37,42};
		boolean b = true;
		
		System.out.print("[");
		for(int i=0;i<6;i++) {
			System.out.print(" "+lottozahlen[i]);
		}
		System.out.println(" ]");
		
		for(int i=0;i<6;i++) {
			if(lottozahlen[i]==12) 
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
			if(lottozahlen[i]==13) 
				b = false;
		}
		if(b)
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");	
	}
}
